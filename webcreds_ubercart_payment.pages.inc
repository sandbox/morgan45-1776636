<?php
function uc_webcreds_complete($cart_id = 0, $report = 0) {
    $ipLIST = variable_get('uc_webcreds_ip_list', '130.193.67.50, 130.193.67.50');
    $report = $_GET['status'];
    switch ($report) {
        case 'true' :
            watchdog('webcreds', 'Пользователь просматривает страницу уведомления оплаты заказа номер !order_id.', array('!order_id' => check_plain($_GET['order'])));
            $order = uc_order_load($_GET['order']);
            if (uc_order_status_data($order -> order_status, 'state') == 'post_checkout') {
                return t('Оплата получена. Ваш заказ ожидает обработки.');
            } else {
                return t('Оплата не была получена. Если вы уверены, что это не так, свяжитесь с администрацией сайта.');
            }
            $leFUNK = uc_cart_get_id(FALSE);
            uc_cart_empty($leFUNK);
            break;
        case 'false' :
            watchdog('webcreds', 'Проблема в ходе оплаты заказа !order_id.', array('!order_id' => check_plain($_GET['order'])));
            return t('Оплата не была получена. Если вы уверены, что это не так, свяжитесь с администрацией сайта.');
            break;
        case 'callback' :
            watchdog('webcreds', 'Получение данных для заказа номер !order_id.', array('!order_id' => check_plain($_POST['PAYNUMBER'])));
            if (trim($ipLIST) != 'noIPs' && trim($ipLIST) != '') {
                $l = explode(",", $ipLIST);
                foreach ($l as $key => $value) {
                    $resultIPs[] = $value;
                }
                if (!in_array($_SERVER['REMOTE_ADDR'], $resultIPs)) {
                    die('IP is not in IP LIST!');
                    header('HTTP_STATUS: 404');
                }
            }
            $SECRET_WORD = variable_get('uc_webcreds_SECRET_WORD', 'qweqwe');
            $order = uc_order_load($_POST['PAYNUMBER']);
            $cTotal = number_format($order -> order_total, 2, '.', '');
            $key = trim($_POST['SIGNATURE']);
            $valid = md5(trim($_POST['AMOUNT']) . trim($_POST['PAYNUMBER']) . trim($_POST['PAYMENT_METHOD_ID']) . $SECRET_WORD);
            if ($key == $valid) {
                $comment = 'Оплачено с помощью WebCreds Checkout. Номер заказа ' . $_POST['PAYNUMBER'];
                $status = uc_order_state_default('post_checkout');
                if (uc_order_update_status($order -> order_id, $status)) {
                    $order -> order_status = $status;
                }
            } else {
                uc_order_comment_save($order -> order_id, 0, t('Попытка нарушить безопасность платежа!'), 'admin');
                watchdog('webcreds', 'Попытка нарушить безопасность платежа. Платеж !order_id.', array('!order_id' => check_plain($_POST['PAYNUMBER'])));
                return MENU_ACCESS_DENIED;
            }
            uc_order_save($order);
            return t('Оплата подтверждена. Перенаправление на страницу завершения оплаты.');
            break;
    }

}
?>